# Check DNS

The Check DNS module simply prevents user registration with an invalid
email domain on the user registration form.


## Table of contents

- Introduction
- Requirements
- Installation
- Configuration


## Introduction

It validates the email domain before registration and checks 
if the domain exists. This prevents false registration.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/extending-drupal/installing-drupal-modules
for further information.


## Configuration

The module has no menu or modifiable settings. There is no configuration.
